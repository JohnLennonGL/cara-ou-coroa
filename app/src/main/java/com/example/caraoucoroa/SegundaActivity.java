package com.example.caraoucoroa;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

public class SegundaActivity extends AppCompatActivity {

    private ImageButton BotaoVoltar;
    private ImageView imagem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda);


        BotaoVoltar = findViewById(R.id.VoltarCoroaID);

        imagem = findViewById(R.id.ImagemID);



      Bundle dados = getIntent().getExtras();
      String escolhaUsuario = dados.getString("escolha");

       if(escolhaUsuario.equals("cara")) {
           imagem.setImageResource(R.drawable.moeda_cara);
       }
       else {
           imagem.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.moeda_coroa));
       }

        BotaoVoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
