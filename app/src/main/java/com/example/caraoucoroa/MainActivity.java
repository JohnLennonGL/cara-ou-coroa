package com.example.caraoucoroa;


import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private ImageButton BotaoJogar;
    private String[] opcao = {"cara","coroa"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BotaoJogar = findViewById(R.id.BotaoJogarID);

        BotaoJogar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                
                int numeroAleatorio =  new Random().nextInt(2);

                Intent intent = new Intent(getApplicationContext(),SegundaActivity.class);
                intent.putExtra("escolha",opcao[numeroAleatorio]);

                startActivity(intent);

            }
        });
    }

}


